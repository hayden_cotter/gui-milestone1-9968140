﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Task_01_wf
{
    public partial class Form1 : Form
    {
        string output = "";

        public Form1()
        {
            InitializeComponent();
            label1.Text = string1;
            label2.Text = string2;
        }

        private void comboBox1_SelectedIndexChanged(object sender, EventArgs e)
        {
            labelResult.Text = choice(textBoxInput.Text, comboBoxChoice.SelectedIndex.ToString());
        }

        /*******************PROGRAM LOGIC*******************/
        public static string string1 = "Type in the number you want to convert:";
        public static string string2 = "Do you want to convert to KMs or Miles?";

        public static string choice(string number, string selection)
        {
            string answer = "";
            int selectionInt = 0;
            double numberDouble = 0.0;

            if (int.TryParse(selection, out selectionInt) && double.TryParse(number, out numberDouble))
            {
                switch (selectionInt)
                {
                    default:
                        answer = "Please select either KMs or Miles.\n";
                        break;
                    case 0:
                        answer = $"{numberDouble} Miles =  {milesToKms(numberDouble)} KMs\n";
                        break;
                    case 1:
                        answer = $"{numberDouble} KMs =  {kmsToMiles(numberDouble)} Miles\n";
                        break;
                }
            }
            else
            {
                if (!int.TryParse(selection, out selectionInt))
                {
                    answer += "Please select either KMs or Miles.\n";
                }
                if (!double.TryParse(number, out numberDouble))
                {
                    answer += "Please enter a valid number.\n";
                }
            }
            return answer;
        }

        public static double kmsToMiles(double input)
        {
            double miles = 0;
            miles = 0.62137119 * input;

            var output = Math.Round(miles, 2);
            return output;
        }

        public static double milesToKms(double input)
        {
            double km = 0;
            km = 1.609344 * input;

            var output = Math.Round(km, 2);
            return output;
        }
        /*****************END PROGRAM LOGIC*****************/

    }
}
