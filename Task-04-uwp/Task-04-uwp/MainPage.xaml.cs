﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Runtime.InteropServices.WindowsRuntime;
using Windows.Foundation;
using Windows.Foundation.Collections;
using Windows.UI.ViewManagement;
using Windows.UI.Xaml;
using Windows.UI.Xaml.Controls;
using Windows.UI.Xaml.Controls.Primitives;
using Windows.UI.Xaml.Data;
using Windows.UI.Xaml.Input;
using Windows.UI.Xaml.Media;
using Windows.UI.Xaml.Navigation;

// The Blank Page item template is documented at http://go.microsoft.com/fwlink/?LinkId=402352&clcid=0x409

namespace Task_04_uwp
{
    /// <summary>
    /// An empty page that can be used on its own or navigated to within a Frame.
    /// </summary>
    public sealed partial class MainPage : Page
    {
        public MainPage()
        {
            this.InitializeComponent();

            fillDictionary();

            textBlock1.Text = string1;

            //Sets window size and title on desktop
            ApplicationView.PreferredLaunchViewSize = new Size { Height = 152, Width = 672 };
            ApplicationView.PreferredLaunchWindowingMode = ApplicationViewWindowingMode.PreferredLaunchViewSize;
            ApplicationView.GetForCurrentView().Title = "Fruit/Veg";
        }

        private void buttonCheck_Click(object sender, RoutedEventArgs e)
        {
            textBlockOutput.Text = select(textBoxInput.Text);
        }

        /*******************PROGRAM LOGIC*******************/
        public static string string1 = "Which Fruit or Vegetable would you like to check for?";
        public static Dictionary<string, string> fruitAndVeg = new Dictionary<string, string>();

        public static void fillDictionary()
        {
            fruitAndVeg.Add("apple", "apple");
            fruitAndVeg.Add("banana", "banana");
            fruitAndVeg.Add("carrot", "carrot");
        }

        public static string select(string input)
        {
            string output = "";

            if (fruitAndVeg.ContainsKey(input.ToLower()))
            {
                output = $"{input} was in the list of Fruits and Vegetables.";
            }
            else
            {
                output = "That Fruit or Vegetable was not in the list.";
            }
            return output;
        }
        /*****************END PROGRAM LOGIC*****************/
    }
}
