﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Runtime.InteropServices.WindowsRuntime;
using Windows.Foundation;
using Windows.Foundation.Collections;
using Windows.UI.Xaml;
using Windows.UI.Xaml.Controls;
using Windows.UI.Xaml.Controls.Primitives;
using Windows.UI.Xaml.Data;
using Windows.UI.Xaml.Input;
using Windows.UI.Xaml.Media;
using Windows.UI.Xaml.Navigation;

// The Blank Page item template is documented at http://go.microsoft.com/fwlink/?LinkId=402352&clcid=0x409

namespace Task_05_uwp
{
    /// <summary>
    /// An empty page that can be used on its own or navigated to within a Frame.
    /// </summary>
    public sealed partial class MainPage : Page
    {
        public MainPage()
        {
            this.InitializeComponent();
            textBlock1.Text = string1;
        }

        private void buttonCheck_Click(object sender, RoutedEventArgs e)
        {
                textBlockOutput.Text = guessRound(textBoxInput.Text);
        }

        /*******************PROGRAM LOGIC*******************/
        public static string string1 = "The program will pick a number from 1 to 5, 5 times, enter in your 5 guesses for what the numbers are:";
        public static int score = 0;
        public static int rounds = 0;
        public static int numComp = 0;
        public static Random rnd = new Random();
        public static int numPlay = 0;
        public static string output = "";

        public static string guessRound(string input)
        {
            if (rounds < 5)
            {
                output = "";

                numPlay = 0;

                numComp = rnd.Next(1, 6);

                if (int.TryParse(input, out numPlay) && numPlay >= 1 && numPlay <= 5)
                {
                    rounds++;
                    if (numPlay == numComp)
                    {
                        output = $"{rounds}: Correct!";
                        score++;
                    }
                    else {
                        output = $"{rounds}: Incorrect.";
                    }
                }
                else
                {
                    output = "Please choose a number from 1 to 5";
                }

                if (rounds == 5)
                {
                    output += $"\nYour final score was: {score}";
                }
                return output;
            }
            return "Game Over";
        }
        /*****************END PROGRAM LOGIC*****************/
    }
}
