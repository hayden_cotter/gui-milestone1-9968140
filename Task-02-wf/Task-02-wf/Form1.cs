﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Task_02_wf
{
    public partial class Form1 : Form
    {
        
        public Form1()
        {
            InitializeComponent();
            label1.Text = string1;
        }

        private void textBoxInput_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Return)
            {
                labelTotal.Text = totalPrice(textBoxInput.Text);
                textBoxInput.Text = "";
            }
        }

        /*******************PROGRAM LOGIC*******************/
        public static string string1 = "Type the price of a item then hit enter\n and repeat to calculate a total plus GST.";
        public static float total = 0;

        public static string totalPrice(string input)
        {
            string output = "";
            float price = 0.0F;
            if (float.TryParse(input, out price))
            {
                total += (price * 1.15F);
                output = "$" + total;
            }
            else
            {
                output = "Please enter a number.";
            }

            return output;
        }
        /*****************END PROGRAM LOGIC*****************/
    }
}
