﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Task_02_console
{
    class Program
    {
        static void Main(string[] args)
        {
            Console.WriteLine(string1);

            for (int i = 0; i<=10; i++) { 
                Console.WriteLine(totalPrice(Console.ReadLine()));
            }
        }

        /*******************PROGRAM LOGIC*******************/
        public static string string1 = "Type the price of a item then hit enter and repeat to calculate a total plus GST.";
        public static float total = 0;

        public static string totalPrice(string input)
        {
            string output = "";
            float price = 0.0F;
            if (float.TryParse(input, out price))
            {
                total += (price * 1.15F);
                output = "$" + total;
            }
            else
            {
                output = "Please enter a number.";
            }
            
            return output;
        }
        /*****************END PROGRAM LOGIC*****************/
    }
}
