﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Task_03_wf
{
    public partial class Form1 : Form
    {
        public Form1()
        {
            InitializeComponent();
            label1.Text = string1;
            listBoxChoices.Items.Add(sonicString);
            listBoxChoices.Items.Add(linkString);
            listBoxChoices.Items.Add(spyroString);
        }

        private void listBoxChoices_SelectedIndexChanged(object sender, EventArgs e)
        {
            labelOutput.Text = select(listBoxChoices.SelectedIndex.ToString());
        }

        /*******************PROGRAM LOGIC*******************/
        public static string string1 = "Who would you like to know more about?";
        public static string sonicString = "0: Sonic";
        public static string linkString = "1: Link";
        public static string spyroString = "2: Spyro";
        public static string error1 = "Please select either 0, 1 or 2";

        public static string select(string input)
        {
            int selectionInt = 0;
            string output = "";

            if (int.TryParse(input, out selectionInt))
            {
                switch (selectionInt)
                {
                    default:
                        output = error1;
                        break;
                    case 0:
                        output = "Sonic is a blue anthropomorphic hedgehog.\nHe first appeared in video games in 1991.";
                        break;
                    case 1:
                        output = "Link appears in different games as a child, teenager or adult.\nLink is from the fictional land of Hyrule.";
                        break;
                    case 2:
                        output = "Spyro is a violet dragon.\nHe first appeared in 1998.";
                        break;
                }
            }
            else
            {
                output = error1;
            }
            return output;
        }
        /*****************END PROGRAM LOGIC*****************/
    }
}
