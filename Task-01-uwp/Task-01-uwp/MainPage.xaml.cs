﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Runtime.InteropServices.WindowsRuntime;
using Windows.Foundation;
using Windows.Foundation.Collections;
using Windows.UI.ViewManagement;
using Windows.UI.Xaml;
using Windows.UI.Xaml.Controls;
using Windows.UI.Xaml.Controls.Primitives;
using Windows.UI.Xaml.Data;
using Windows.UI.Xaml.Input;
using Windows.UI.Xaml.Media;
using Windows.UI.Xaml.Navigation;

// The Blank Page item template is documented at http://go.microsoft.com/fwlink/?LinkId=402352&clcid=0x409

namespace Task_01_uwp
{
    /// <summary>
    /// An empty page that can be used on its own or navigated to within a Frame.
    /// </summary>
    public sealed partial class MainPage : Page
    {
        string output = "";

        public MainPage()
        {
            this.InitializeComponent();

            //Sets window size and title on desktop
            ApplicationView.PreferredLaunchViewSize = new Size { Height = 150, Width = 260 };
            ApplicationView.PreferredLaunchWindowingMode = ApplicationViewWindowingMode.PreferredLaunchViewSize;
            ApplicationView.GetForCurrentView().Title = "Converter";
        }

        private void buttonMilestoKms_Click(object sender, RoutedEventArgs e)
        {
            textBlockResult.Text = choice(textBoxInput.Text, "0");
        }

        private void buttonKmsToMiles_Click(object sender, RoutedEventArgs e)
        {
            textBlockResult.Text = choice(textBoxInput.Text, "1");
        }

        /*******************PROGRAM LOGIC*******************/
        public static string string1 = "Type in the number you want to convert:";
        public static string string2 = "Do you want to convert to 0: KMs or 1: Miles?";

        public static string choice(string number, string selection)
        {
            string answer = "";
            int selectionInt = 0;
            double numberDouble = 0.0;

            if (int.TryParse(selection, out selectionInt) && double.TryParse(number, out numberDouble))
            {
                switch (selectionInt)
                {
                    default:
                        answer = "Please select either KMs or Miles.\n";
                        break;
                    case 0:
                        answer = $"{numberDouble} Miles =  {milesToKms(numberDouble)} KMs\n";
                        break;
                    case 1:
                        answer = $"{numberDouble} KMs =  {kmsToMiles(numberDouble)} Miles\n";
                        break;
                }
            }
            else
            {
                if (!int.TryParse(selection, out selectionInt))
                {
                    answer += "Please select either KMs or Miles.\n";
                }
                if (!double.TryParse(number, out numberDouble))
                {
                    answer += "Please enter a valid number.\n";
                }
            }
            return answer;
        }

        public static double kmsToMiles(double input)
        {
            double miles = 0;
            miles = 0.62137119 * input;

            var output = Math.Round(miles, 2);
            return output;
        }

        public static double milesToKms(double input)
        {
            double km = 0;
            km = 1.609344 * input;

            var output = Math.Round(km, 2);
            return output;
        }
        /*****************END PROGRAM LOGIC*****************/

    }
}
