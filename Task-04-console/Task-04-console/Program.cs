﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Task_04_console
{
    class Program
    {
        static void Main(string[] args)
        {
            fillDictionary();

            Console.WriteLine(string1);

            Console.WriteLine(select(Console.ReadLine()));

            Console.ReadKey();
        }

        /*******************PROGRAM LOGIC*******************/
        public static string string1 = "Which Fruit or Vegetable would you like to check for?";
        public static Dictionary<string, string> fruitAndVeg = new Dictionary<string, string>();

        public static void fillDictionary()
        {
            fruitAndVeg.Add("apple", "apple");
            fruitAndVeg.Add("banana", "banana");
            fruitAndVeg.Add("carrot", "carrot");
        }

        public static string select(string input)
        {
            string output = "";
         
            if (fruitAndVeg.ContainsKey(input.ToLower()))
            {
                output = $"{input} was in the list of Fruits and Vegetables.";
            }
            else
            {
                output = "That Fruit or Vegetable was not in the list.";
            }
            return output;
        }
        /*****************END PROGRAM LOGIC*****************/
    }
}
