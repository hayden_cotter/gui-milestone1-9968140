﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Task_05_console
{
    class Program
    {
        static void Main(string[] args)
        {
            Console.WriteLine(string1);
            while (rounds < 5) {
                Console.WriteLine(guessRound(Console.ReadLine()));
            }
            Console.ReadKey();
        }

        /*******************PROGRAM LOGIC*******************/
        public static string string1 = "The program will pick a number from 1 to 5, 5 times, enter in your 5 guesses for what the numbers are:";
        public static int score = 0;
        public static int rounds = 0;
        public static int numComp = 0;
        public static Random rnd = new Random();
        public static int numPlay = 0;
        public static string output = "";

        public static string guessRound(string input)
        {
            if (rounds < 5)
            {
                output = "";

                numPlay = 0;

                numComp = rnd.Next(1, 6);

                if (int.TryParse(input, out numPlay) && numPlay >= 1 && numPlay <= 5)
                {
                    rounds++;
                    if (numPlay == numComp)
                    {
                        output = $"{rounds}: Correct!";
                        score++;
                    }
                    else {
                        output = $"{rounds}: Incorrect.";
                    }
                }
                else
                {
                    output = "Please choose a number from 1 to 5";
                }

                if (rounds == 5)
                {
                    output += $"\nYour final score was: {score}";
                }
                return output;
            }
            return "Game Over";
        }
        /*****************END PROGRAM LOGIC*****************/
    }
}
