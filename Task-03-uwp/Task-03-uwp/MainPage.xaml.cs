﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Runtime.InteropServices.WindowsRuntime;
using Windows.Foundation;
using Windows.Foundation.Collections;
using Windows.UI.ViewManagement;
using Windows.UI.Xaml;
using Windows.UI.Xaml.Controls;
using Windows.UI.Xaml.Controls.Primitives;
using Windows.UI.Xaml.Data;
using Windows.UI.Xaml.Input;
using Windows.UI.Xaml.Media;
using Windows.UI.Xaml.Navigation;

// The Blank Page item template is documented at http://go.microsoft.com/fwlink/?LinkId=402352&clcid=0x409

namespace Task_03_uwp
{
    /// <summary>
    /// An empty page that can be used on its own or navigated to within a Frame.
    /// </summary>
    public sealed partial class MainPage : Page
    {
        public MainPage()
        {
            this.InitializeComponent();
            listBoxChoices.Items.Add(sonicString);
            listBoxChoices.Items.Add(linkString);
            listBoxChoices.Items.Add(spyroString);

            textBlock.Text = string1;

            //Sets window size and title on desktop
            ApplicationView.PreferredLaunchViewSize = new Size { Height = 152, Width = 672 };
            ApplicationView.PreferredLaunchWindowingMode = ApplicationViewWindowingMode.PreferredLaunchViewSize;
            ApplicationView.GetForCurrentView().Title = "Character Info";
        }

        private void listBoxChoices_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            textBlockOutput.Text = select(listBoxChoices.SelectedIndex.ToString());
        }

        /*******************PROGRAM LOGIC*******************/
        public static string string1 = "Who would you like to know more about?";
        public static string sonicString = "0: Sonic";
        public static string linkString = "1: Link";
        public static string spyroString = "2: Spyro";
        public static string error1 = "Please select either 0, 1 or 2";

        public static string select(string input)
        {
            int selectionInt = 0;
            string output = "";

            if (int.TryParse(input, out selectionInt))
            {
                switch (selectionInt)
                {
                    default:
                        output = error1;
                        break;
                    case 0:
                        output = "Sonic is a blue anthropomorphic hedgehog.\nHe first appeared in video games in 1991.";
                        break;
                    case 1:
                        output = "Link appears in different games as a child, teenager or adult.\nLink is from the fictional land of Hyrule.";
                        break;
                    case 2:
                        output = "Spyro is a violet dragon.\nHe first appeared in 1998.";
                        break;
                }
            }
            else
            {
                output = error1;
            }
            return output;
        }
        /*****************END PROGRAM LOGIC*****************/
    }
}
