﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Runtime.InteropServices.WindowsRuntime;
using Windows.Foundation;
using Windows.Foundation.Collections;
using Windows.UI.ViewManagement;
using Windows.UI.Xaml;
using Windows.UI.Xaml.Controls;
using Windows.UI.Xaml.Controls.Primitives;
using Windows.UI.Xaml.Data;
using Windows.UI.Xaml.Input;
using Windows.UI.Xaml.Media;
using Windows.UI.Xaml.Navigation;

// The Blank Page item template is documented at http://go.microsoft.com/fwlink/?LinkId=402352&clcid=0x409

namespace Task_02_uwp
{
    /// <summary>
    /// An empty page that can be used on its own or navigated to within a Frame.
    /// </summary>
    public sealed partial class MainPage : Page
    {
        public MainPage()
        {
            this.InitializeComponent();

            textBlock1.Text = string1;

            //Sets window size and title on desktop
            ApplicationView.PreferredLaunchViewSize = new Size { Height = 160, Width = 300 };
            ApplicationView.PreferredLaunchWindowingMode = ApplicationViewWindowingMode.PreferredLaunchViewSize;
            ApplicationView.GetForCurrentView().Title = "Price Calculator";
        }

        private void buttonAdd_Click(object sender, RoutedEventArgs e)
        {
            textBlockTotal.Text = totalPrice(textBoxInput.Text);
            textBoxInput.Text = "";
        }

        /*******************PROGRAM LOGIC*******************/
        public static string string1 = "Type the price of a item then click add\n repeat to calculate a total plus GST.";
        public static float total = 0;

        public static string totalPrice(string input)
        {
            string output = "";
            float price = 0.0F;
            if (float.TryParse(input, out price))
            {
                total += (price * 1.15F);
                output = "$" + total;
            }
            else
            {
                output = "Please enter a number.";
            }

            return output;
        }
        /*****************END PROGRAM LOGIC*****************/

    }
}
