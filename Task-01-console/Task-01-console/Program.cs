﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Task_01_console
{
    class Program
    {
        static void Main(string[] args)
        {
            Console.WriteLine(string1);
            string number = Console.ReadLine();

            Console.WriteLine(string2);
            Console.WriteLine(choice(number, Console.ReadLine()));

            Console.ReadKey();
        }

        /*******************PROGRAM LOGIC*******************/
        public static string string1 = "Type in the number you want to convert:";
        public static string string2 = "Do you want to convert to 0: KMs or 1: Miles?";

        public static string choice(string number, string selection)
        {
            string answer = "";
            int selectionInt = 0;
            double numberDouble = 0.0;
            
            if (int.TryParse(selection, out selectionInt) && double.TryParse(number, out numberDouble))
            {
                switch (selectionInt)
                {
                    default:
                        answer = "Please select either 0: KMs or 1: Miles.\n";
                        break;
                    case 0:
                        answer = $"{numberDouble} Miles =  {milesToKms(numberDouble)} KMs\n";
                        break;
                    case 1:
                        answer = $"{numberDouble} KMs =  {kmsToMiles(numberDouble)} Miles\n";
                        break;
                }
            }
            else
            {
                if (!int.TryParse(selection, out selectionInt))
                {
                    answer += "Please select either 0: KMs or 1: Miles.\n";
                }
                if (!double.TryParse(number, out numberDouble))
                {
                    answer += "Please enter a valid number.\n";
                }
            }
            return answer;
        }

        public static double kmsToMiles(double input)
        {
            double miles = 0;
            miles = 0.62137119 * input;

            var output = Math.Round(miles, 2);
            return output;
        }

        public static double milesToKms(double input)
        {
            double km = 0;
            km = 1.609344 * input;

            var output = Math.Round(km, 2);
            return output;
        }
        /*****************END PROGRAM LOGIC*****************/
    }
}
